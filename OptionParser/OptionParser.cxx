#include "OptionParser.h"

std::vector<std::string> tools::getopt::Option::args = std::vector<std::string>();

tools::getopt::Option::Option(const std::string &longFlag, unsigned argumentCount, const std::string &shortFlag, const std::string &description, bool isMandatory) noexcept
: longFlag(longFlag), argumentCount(argumentCount), shortFlag(shortFlag), isMandatory(isMandatory)
{}

bool tools::getopt::Option::isActive(void) const noexcept
{
    for(unsigned i =1;i<args.size();++i)
        if(args[i] == longFlag || args[i] == shortFlag)
            return true;
    return false;
}

void tools::getopt::init(int argc, const char *argv[])
{
    tools::getopt::Option::args.resize(argc);
    for(int i = 0;i<argc;++i)
        tools::getopt::Option::args[i] = std::string(argv[i]);
}

