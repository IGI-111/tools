#ifndef __OPTION_PARSER_H__
#define __OPTION_PARSER_H__

#include <string>
#include <vector>

namespace tools {
namespace getopt {

class Option
{
public:
    Option(const std::string &longFlag, unsigned argumentCount = 0, const std::string &shortFlag = "", const std::string &description = "", bool isMandatory = false) noexcept;
    bool isActive(void) const noexcept;
    inline bool hasArguments(void) const noexcept;
    template <typename T>
        std::vector<T> getArguments(void) const;
    inline std::string getName(void) const noexcept;
    inline std::string getDescription(void) const noexcept;
private:
    static std::vector<std::string> args;
    friend void init(int argc, const char *argv[]);

    std::string longFlag;
    unsigned argumentCount;
    std::string shortFlag;
    std::string description;
    bool isMandatory;
};


void init(int argc, const char *argv[]);

} // namespace getopt
} // namespace tools

#include "OptionParser.hxx"

#endif
