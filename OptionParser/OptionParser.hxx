#ifndef __OPTION_PARSER_HXX__
#define __OPTION_PARSER_HXX__

#include <vector>
#include <stdexcept>
#include "OptionParser.h"


bool tools::getopt::Option::hasArguments(void) const noexcept
{
    return argumentCount > 0;
}

template<typename T> std::vector<T> tools::getopt::Option::getArguments(void) const
{
    std::vector<T> arguments;
    for (int i = 1; i < args.size(); ++i)
        if (args[i] == longFlag || args[i] == shortFlag)
        {
            for (unsigned j = 1; j <= argumentCount && i+j < args.size(); ++j)
                arguments.push_back(T(args[i + j]));
            return arguments;
        }
    if(isMandatory)
        throw std::runtime_error("Mandatory option " + longFlag + " not set.");
    return arguments;
}

inline std::string tools::getopt::Option::getName(void) const noexcept
{
    return longFlag;
}

inline std::string tools::getopt::Option::getDescription(void) const noexcept
{
    return description;
}

#endif /* end of include guard */
