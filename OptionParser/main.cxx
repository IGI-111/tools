#include <iostream>
#include "OptionParser.h"

int main(int argc, char const* argv[])
{
    using namespace std;
    using namespace tools;
    getopt::init(argc, argv);
    vector<getopt::Option> opts =
    {
        getopt::Option("--help", 0, "-h", "Displays the help dialog."),
        getopt::Option("--two-argument-option", 2, "-o2", "A two argument option"),
        getopt::Option("--one-argument-option", 1, "-o1", "A one argument option")
    };

    for (getopt::Option opt : opts)
    {
        cout << opt.getName() << ' ' << opt.getDescription() << endl;
        cout << "The option is " << (opt.isActive() ? "active" : "not active") << '.' << endl;
        if (opt.hasArguments())
        {
            cout << "Passed as arguments: ";
            for (string str : opt.getArguments<string>())
                cout << str << ' ';
            cout << endl;
        }
        cout << endl;
    }

    return 0;
}
